﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Тестовый скрипт для проверки рисования иконок 
/// </summary>
public class IconSample : MonoBehaviour {

	void OnDrawGizmos ()
	{
	    Gizmos.DrawIcon(transform.position, "clock.png", true);

	}
	
}
