﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.GU_21_02_2018
{
    /// <summary>
    /// Тестовый скрипт для демонстрации возможностей кастомного отображения свойств в инспекторе объектов
    /// </summary>
    [System.Serializable]
    [RequireComponent(typeof(SampleComponent2))]
    [ExecuteInEditMode]
    public class InspectorTestScript : MonoBehaviour
    {

        [HideInInspector] public int SomeIntValue;

        [SerializeField] private int SomeVisibleField;

        [Header("Примечание для поля")] public int SomeImportantValue;

        [Range(0, 100)] public int SomeRangeValue;

        [Space] public int SomeIndentValue;

        [Multiline(10)] public string SomeTextData;

        [TextArea(3, 5)] public string SomeTextArea;

        [Tooltip("Подсказка для поля")] public int SomeHintValue;

        public Rigidbody SomeRigidbody;


        /*
        [ContextMenuItem("Random Count", nameof(Random))]
        [SerializeField] private int _count;

        private void Random()
        {
            _count = UnityEngine.Random.Range(0, 100);
        }
        */

        // Use this for initialization
        [Obsolete]
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}