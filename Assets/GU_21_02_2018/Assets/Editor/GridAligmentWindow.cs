﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Geekbrains.Editor
{
    public class GridAligmentWindow : EditorWindow
    {
        public GameObject PrefabGameObject;
        public string captionTemplate;
        public int rows;
        public int cols;

        public int colsIndent = 10;
        public int rowsIndent = 10;
        bool groupEnabled;

        [MenuItem("Geekbrains/GridAligment")]
        public static void ShowWindow()
        {
            // Отобразить существующий экземпляр окна. Если его нет, создаем
            EditorWindow.GetWindow(typeof(GridAligmentWindow));
        }

        [MenuItem("Geekbrains/Delete objscts")]
        public static void DeleteObjects()
        {
            Debug.Log("Try to delete objects");

            //var objects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            //foreach (var obj in objects)
            //{
            //    Debug.Log(obj.name);
            //}

            //object[] obj = GameObject.FindSceneObjectsOfType(typeof(GameObject));
            //foreach (object o in obj)
            //{
            //    GameObject g = (GameObject)o;
            //    Debug.Log(g.name);
            //}

            var objects = GameObject.FindObjectsOfType(typeof(GameObject));
            foreach (var obj in objects)
            {
                //Destroy(obj);
                if (obj.name.Contains("test_"))
                {
                    Debug.Log(obj.name);
                    DestroyImmediate(obj);
                }
                
            }

            //foreach (GameObject go in Resources.FindObjectsOfTypeAll<GameObject>())
            //{
            //    if (go.name == "test") Debug.Log(go.name);
            //}
        }

        void OnGUI()
        {
            // Здесь методы отрисовки схожи с методами в пользовательском интерфейсе, который вы разрабатывали на курсе “Unity3D.Уровень 1”
            GUILayout.Label("Базовые настройки", EditorStyles.boldLabel);
            PrefabGameObject = EditorGUILayout.ObjectField("Объект для размещения в сетке", PrefabGameObject,
                typeof(GameObject), true) as GameObject;
            captionTemplate = EditorGUILayout.TextField("Шаблон имени объекта", captionTemplate);
            rows = EditorGUILayout.IntSlider("Количество строк", rows, 1, 10);
            cols = EditorGUILayout.IntSlider("Количество столбцов", cols, 1, 10);
            groupEnabled = EditorGUILayout.BeginToggleGroup("Дополнительные настройки", groupEnabled);
            rowsIndent = EditorGUILayout.IntSlider("Отступ между строкам", colsIndent, 1, 100);
            colsIndent = EditorGUILayout.IntSlider("Отступ между столбцами", cols, 1, 100);
            EditorGUILayout.EndToggleGroup();
            if (GUILayout.Button("Расположить"))
            {
                if (PrefabGameObject)
                {
                    GameObject parent = new GameObject("ObjectGroup");
                    for (int i = 0; i < rows; i++)
                    {
                        for (int j = 0; j < cols; j++)
                        {
                            Vector3 pos = new Vector3(i * colsIndent, j * rowsIndent, 0);
                            GameObject obj = Instantiate(PrefabGameObject, pos, Quaternion.identity) as GameObject;
                            obj.transform.parent = parent.transform;
                            obj.name = captionTemplate + "_" + i + "_" + j;
                        }
                    }

                }
            }
        }
    }
}
