﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Geekbrains.Editor
{
    // здесь показан пример того, как можно в инспекторе объектов кастомизировать отображение свойств компонента (скрипта),
    // добавив к нему дополнительные элементы (окна, кнопки и т. п.)
    // Испорльзуется совместно с MyScript
    [CustomEditor(typeof(MyScript))]
    public class MyScriptЕditor : UnityEditor.Editor
    {
        bool _isPressButtonOk;
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            MyScript testTarget = (MyScript)target;
            var isPressButton = GUILayout.Button("Создание объектов по кнопке", EditorStyles.miniButton);
            if (isPressButton)
            {
                testTarget.CreateObj();
                _isPressButtonOk = true;
            }
            if (_isPressButtonOk)
            {
                EditorGUILayout.HelpBox("Вы нажали на кнопку", MessageType.Warning);
            }
        }
    }
}
