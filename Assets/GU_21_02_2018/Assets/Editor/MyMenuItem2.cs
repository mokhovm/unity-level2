﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Geekbrains.Editor
{
    /// <summary>
    /// Пример того, как и куда можно добавлять пункты меню
    /// </summary>
    public class MyMenuItem2 : MonoBehaviour
    {

        [MenuItem("Geekbrains/Пункт меню №0 ")]
        private static void MenuOption()
        {
            Debug.Log("Пункт меню №0");
        }
        [MenuItem("Geekbrains/Пункт меню №1 %#a")]
        private static void NewMenuOption()
        {
            Debug.Log("Пункт меню №1");
        }
        [MenuItem("Geekbrains/Пункт меню №2 %g")]
        private static void NewNestedOption()
        {
            Debug.Log("Пункт меню №2");
        }
        [MenuItem("Geekbrains/Пункт меню №3 _g")]
        private static void NewOptionWithHotkey()
        {
            Debug.Log("Пункт меню №3");
        }

        [MenuItem("Assets/Geekbrains")]
        private static void LoadAdditiveScene()
        {
            Debug.Log("Assets/Geekbrains");
        }

        [MenuItem("Assets/Create/Geekbrains")]
        private static void AddConfig()
        {
            Debug.Log("Assets/Create/Geekbrains");
        }

        [MenuItem("CONTEXT/Rigidbody/Geekbrains")]
        private static void NewOpenForRigidBody()
        {
            Debug.Log("CONTEXT/Rigidbody/Geekbrains");
        }



    }
}