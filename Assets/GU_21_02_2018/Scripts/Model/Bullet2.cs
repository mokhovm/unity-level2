﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.GU_21_02_2018
{
    /// <summary>
    /// Пуля 2
    /// </summary>
    class Bullet2 : BaseAmmunition
    {
        private const int DEF_DMG_VALUE = 50;
        private const int DEF_DMG_RADIUS = 10;
        private const int DEF_DMG_POWER = 100;

        private void OnCollisionEnter(UnityEngine.Collision collision)
        {
            var obj = collision.gameObject.GetComponent<IDestructable>();
            if (obj != null)
            {
                SetDamage(collision.gameObject.GetComponent<IDestructable>());
                Destroy(InstanceObject);

            }
        }

        private void SetDamage(IDestructable obj)
        {
            if (obj != null) obj.updateHealth(new CollisionInfo(_currentDamage, transform.forward));
        }

        override protected void DestroyAmmunition()
        {
            Explode();
            base.DestroyAmmunition();
        }


        protected void Explode()
        {
            Debug.Log("boom!");
            Collider[] colliders = Physics.OverlapSphere(this.transform.position, DEF_DMG_RADIUS);
            foreach (Collider hit in colliders)
            {
                Vector3 direction = hit.transform.position - transform.position;
                if (hit.attachedRigidbody != null)
                {
                    Debug.Log("force");
                    hit.attachedRigidbody.AddForce(direction.normalized * DEF_DMG_POWER);
                }
            }
            Destroy(this.gameObject);
        }


    }
}
