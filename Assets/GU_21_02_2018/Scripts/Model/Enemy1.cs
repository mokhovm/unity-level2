﻿using System;

namespace Assets.GU_21_02_2018
{
    /// <summary>
    /// Уничтожаемый объект
    /// </summary>
	public class Enemy1 : BaseObjectScene, IDestructable
	{
		private float _hp = 50f;
		private float _timeDest = 1f;

		public float health
		{
			get { return _hp; }
			private set { _hp = value; }
		}

		public void updateHealth(CollisionInfo info)
		{
			if (health > 0)
			{
				health -= info.Damage;
			}

			if (health <= 0)
			{
				Destroy(InstanceObject, _timeDest);
			}
		}
	}
}