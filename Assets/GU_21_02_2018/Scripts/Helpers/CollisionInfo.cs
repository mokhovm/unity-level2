﻿using UnityEngine;

namespace Assets.GU_21_02_2018
{
	public struct CollisionInfo
	{
		private readonly float _damage;
		private readonly Vector3 _direction;

		public CollisionInfo(float damage, Vector3 direction)
		{
			_damage = damage;
			_direction = direction;
		}

		public float Damage
		{
			get { return _damage; }
		}

		public Vector3 Direction
		{
			get { return _direction; }
		}
	}
}