﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Assets.GU_21_02_2018
{
    class EnemyWithShield : BaseObjectScene, IDestructable
    {
        private float _hp = 30f;
        private float _timeDest = 1f;
        private float _shield = 20f;


        public float health
        {
            get { return _hp; }
            private set { _hp = value; }
        }

        public float shield
        {
            get { return _shield; }
            set {
                _shield = value;
                if (_shield < 0) _shield = 0;
            }
        }

        public void updateHealth(CollisionInfo info)
        {
            if (shield > 0)
            {
                shield -= info.Damage;
            }
            else if (health > 0)
            {
                health -= info.Damage;
            }

            if (health <= 0)
            {
                Destroy(InstanceObject, _timeDest);
            }
        }

    }
}
