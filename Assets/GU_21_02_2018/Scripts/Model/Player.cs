﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.GU_21_02_2018
{

    public class Player : BaseObjectScene, IDestructable
    {
        private const int DEF_HEALTH = 50;
        private float _health;

        public float health
        {
            get { return _health; }
        }

        override protected void Awake()
        {
            base.Awake();
            _health = DEF_HEALTH;
        }

        private void OnCollisionEnter(UnityEngine.Collision collision)
        {
            var obj = collision.gameObject.GetComponent<IPickable>();
            if (obj != null) obj.pickUp(this);
        }


        public void updateHealth(CollisionInfo info)
        {
            _health -= info.Damage;
        }

    }
}
