﻿using Assets.GU_21_02_2018;

namespace Assets.GU_21_02_2018
{
	public interface IDestructable
	{
		void updateHealth(CollisionInfo info);
		float health { get; }
	}
}