﻿using System;
using UnityEngine;

namespace Assets.GU_21_02_2018
{
	public sealed class InputController : BaseController
	{
	    public const int MIN_WEAPON = 0;
	    public const int MAX_WEAPON = 1;

        private int m_curWeapon = MIN_WEAPON;

	    public int curWeapon
	    {
	        get { return m_curWeapon; }
	        set
	        {
	            m_curWeapon = value;
	            if (m_curWeapon > MAX_WEAPON) m_curWeapon = MAX_WEAPON;
	            if (m_curWeapon < MIN_WEAPON) m_curWeapon = MIN_WEAPON;
                SetWeapon(m_curWeapon);
	        }
	    }

        /*
	    private void Awake()
	    {
	        curWeapon = 0;
	    }
        */



        private void Update()
		{
			if (Input.GetKeyDown(KeyCode.F))
			{
				Main.Instance.FlashLightController.Switch();
			}

		    if (Input.GetAxis("Mouse ScrollWheel") != 0f)
		    {
		        curWeapon += Input.GetAxis("Mouse ScrollWheel") < 0 ? -1 : 1;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SetWeapon(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SetWeapon(1);
            }

        }

        private void SetWeapon(int i)
		{
			Main.Instance.WeaponController.Off();
			var tempWeapon = Main.Instance.ObjectManager.Weapons[i];

			if (tempWeapon)
			{
				Main.Instance.WeaponController.On(tempWeapon);
			}
		}
	}
}