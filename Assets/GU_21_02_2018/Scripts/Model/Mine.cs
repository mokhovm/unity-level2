﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.GU_21_02_2018
{

    public class Mine : BaseObjectScene, IPickable
    {
        private const int DEF_DMG_VALUE = 50;
        private const int DEF_DMG_RADIUS = 20;
        private const int DEF_DMG_POWER = 500;

        public void pickUp(BaseObjectScene actor)
        {
            if (actor is IDestructable)
            {
                Debug.Log("boom!");
                Collider[] colliders = Physics.OverlapSphere(this.transform.position, DEF_DMG_RADIUS);
                foreach (Collider hit in colliders) 
                {
                    Vector3 direction = hit.transform.position - transform.position;
                    if (hit.attachedRigidbody != null)
                    {
                        Debug.Log(hit.attachedRigidbody.name);
                        hit.attachedRigidbody.AddForce(direction.normalized * DEF_DMG_POWER);
                    }
                }
                (actor as IDestructable).updateHealth(new CollisionInfo(DEF_DMG_VALUE, Vector3.zero));
                Destroy(this.gameObject);
            }
        }
    }
}