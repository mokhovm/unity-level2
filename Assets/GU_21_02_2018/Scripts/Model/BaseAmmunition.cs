﻿using UnityEngine;

namespace Assets.GU_21_02_2018
{
    /// <summary>
    /// Базовый класс для снарядов
    /// </summary>
	public abstract class BaseAmmunition : BaseObjectScene
	{
		[SerializeField] protected float _timeToDestruct = 10;
		[SerializeField] protected float _baseDamage = 10;

		protected float _currentDamage;

		protected override void Awake()
		{
			base.Awake();
            Invoke("DestroyAmmunition", _timeToDestruct);
			_currentDamage = _baseDamage;
		}

	    protected virtual void DestroyAmmunition()
	    {
	        Destroy(InstanceObject);
	    }
	}
}