﻿using System.Collections;
using System.Collections.Generic;
using Assets.GU_21_02_2018;
using UnityEngine;

/// <summary>
/// описывает интерфейс взаимодействия с тем, что можно подобрать или на что можно наступить
/// </summary>
public interface IPickable
{
    void pickUp(BaseObjectScene actor);

}
