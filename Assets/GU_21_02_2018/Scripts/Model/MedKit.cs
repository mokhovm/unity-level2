﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.GU_21_02_2018
{
    
    public class MedKit : BaseObjectScene, IPickable
    {
        private const int DEF_HP_VALUE = 50;
        public void pickUp(BaseObjectScene actor)
        {
            if (actor is IDestructable)
            {
                (actor as IDestructable).updateHealth(new CollisionInfo(-DEF_HP_VALUE, Vector3.zero)); 
                Destroy(this.gameObject);
            }
        }
    }
}