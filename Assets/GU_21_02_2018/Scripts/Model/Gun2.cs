﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.GU_21_02_2018
{
    public class Gun2 : BaseWeapon
    {

        //void Awake()
        //{
        //    Name = "Gun1";
        //}


        /// <summary>
        /// Оружие 2
        /// </summary>
        /// <param name="ammunition"></param>
        public override void Fire(BaseAmmunition ammunition)
        {
            if (_isFire)
            {
                if (ammunition)
                {
                    var temp = Instantiate(ammunition, _barrel.position, _barrel.localRotation);
                    temp.Rigidbody.AddForce(_barrel.forward * _force);
                    _isFire = false;
                    _timer.Start(_rechergeTime);
                }
                else
                {
                    // RayCast
                }
            }
        }
    }
}
