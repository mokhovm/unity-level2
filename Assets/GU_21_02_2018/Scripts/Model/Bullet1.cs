﻿using System;

namespace Assets.GU_21_02_2018
{
    /// <summary>
    /// Пуля 1
    /// </summary>
	public class Bullet1 : BaseAmmunition
	{
		private void OnCollisionEnter(UnityEngine.Collision collision)
		{
			SetDamage(collision.gameObject.GetComponent<IDestructable>());
			Destroy(InstanceObject);
		}

		private void SetDamage(IDestructable obj)
		{
			if (obj != null) obj.updateHealth(new CollisionInfo(_currentDamage, transform.forward));
		}
	}
}